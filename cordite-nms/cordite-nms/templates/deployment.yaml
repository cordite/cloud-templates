apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: {{ include "nms.fullname" . }}
  labels:
    app.kubernetes.io/name: {{ include "nms.name" . }}
    helm.sh/chart: {{ include "nms.chart" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
spec:
  replicas: {{ .Values.nms.replicaCount | default "1" }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "nms.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "nms.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
      initContainers:
      - name: init-mongodb
        image: busybox:latest
        command:
          - '/bin/sh'
          - '-c'
          - |
              while true
              do
                rt=$(nc -z -w 1 {{ .Release.Name }}-mongodb 27017)
                if [ $? -eq 0 ]; then
                  echo "Mongo DB is UP"
                  break
                fi
                echo "Mongo DB is not yet reachable;sleep for 10s before retry"
                sleep 10
              done
      containers:
        - name: {{ .Chart.Name }}
          {{- $repo := .Values.nms.image.repository | default "cordite/network-map" }}
          {{- $tag := .Values.nms.image.tag | default "latest" }}
          image: "{{ $repo }}:{{ $tag }}"
          imagePullPolicy: {{ .Values.nms.image.pullPolicy | default "Always" }}
          env:
            - name: NMS_AUTH_PASSWORD
              valueFrom: 
                secretKeyRef:
                  name: {{ template "nms.fullname" . }}
                  key: nms-auth-password
            - name: NMS_AUTH_USERNAME
              value: {{ .Values.nms.env.NMS_AUTH_USERNAME | default "sa" | quote }}
            - name: NMS_CACHE_TIMEOUT
              value: {{ .Values.nms.env.NMS_CACHE_TIMEOUT | default "2S" | quote }}
            - name: NMS_CERTMAN
              value: {{ .Values.nms.env.NMS_CERTMAN | default "true" | quote }}
            - name: NMS_CERTMAN_PKIX
              value: {{ .Values.nms.env.NMS_CERTMAN_PKIX | default "false" | quote }}
            - name: NMS_CERTMAN_STRICT_EV
              value: {{ .Values.nms.env.NMS_CERTMAN_STRICT_EV | default "false" | quote }}
            {{- if .Values.nms.secrets.certman_truststore }}
            - name: NMS_CERTMAN_TRUSTSTORE
              value: "/mnt/cordite/network-map/secrets/nms-certman-truststore.jks"
            {{- end }}
            {{- if .Values.nms.env.NMS_CERTMAN_TRUSTSTORE_PASSWORD }}
            - name: NMS_CERTMAN_TRUSTSTORE_PASSWORD
              value: {{ .Values.nms.env.NMS_CERTMAN_TRUSTSTORE_PASSWORD | default "pass" | quote }}
            {{- end }}
            - name: NMS_DB
              value: {{ .Values.nms.env.NMS_DB | default ".db" | quote }}
            - name: NMS_DOORMAN
              value: {{ .Values.nms.env.NMS_DOORMAN | default "true" | quote }}
            - name: NMS_HOSTNAME
              value: {{ .Values.nms.env.NMS_HOSTNAME | default "0.0.0.0" | quote }}
            - name: NMS_MONGO_CONNECTION_STRING
            {{- if (eq .Values.mongodb.enabled true) }}
              value: "mongodb://{{ .Values.mongodb.mongodbUsername}}:{{ .Values.mongodb.mongodbPassword}}@{{ .Release.Name }}-mongodb/{{ .Values.mongodb.mongodbDatabase}}" 
            {{- else}}
              value: "embed"
            {{- end }}
            - name: NMS_MONGOD_DATABASE
              value: {{ .Values.mongodb.mongodbDatabase | default "nms" | quote }}
            {{- if .Values.nms.env.NMS_MONGOD_LOCATION}}
            - name: NMS_MONGOD_LOCATION
              value: {{ .Values.nms.env.NMS_MONGOD_LOCATION | default "" | quote }}
            {{- end }}
            - name: NMS_NETWORKMAP_DELAY     
              value: {{ .Values.nms.env.NMS_NETWORKMAP_DELAY | default "1S" | quote }}
            - name: NMS_PARAM_UPDATE_DELAY     
              value: {{ .Values.nms.env.NMS_PARAM_UPDATE_DELAY | default "10S" | quote }}
            - name: NMS_PORT
              value: {{ .Values.nms.env.NMS_PORT | default "8443" | quote }}      
            - name: NMS_ROOT_CA_NAME
              value: {{ .Values.nms.env.NMS_ROOT_CA_NAME | default "CN='', OU=Cordite Foundation Network, O=Cordite Foundation, L=London, ST=London, C=GB"  | quote }}  
            {{- if and .Values.nms.secrets.tls_cert .Values.nms.secrets.tls_cert }}
            - name: NMS_TLS
              value: "true"
            {{- else}}
            - name: NMS_TLS
              value: "false"
            {{- end }}
            {{- if .Values.nms.secrets.tls_cert }}
            - name: NMS_TLS_CERT_PATH
              value: "/mnt/cordite/network-map/secrets/nms-tls.cert"
            {{- end }}
            {{- if .Values.nms.secrets.tls_key }}
            - name: NMS_TLS_KEY_PATH
              value: "/mnt/cordite/network-map/secrets/nms-tls.key"
            {{- end }}
            - name: NMS_WEB_ROOT
              value: {{ .Values.nms.env.NMS_WEB_ROOT | default "/"  | quote }}
          ports:
            - name: http
              containerPort: {{ .Values.nms.env.NMS_PORT | default "8443" }}  
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          volumeMounts:
            - name: secrets
              readonly: true
              mountPath: "/mnt/cordite/network-map/secrets"
          resources:
{{ toYaml .Values.resources | indent 12 }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      securityContext:
        runAsUser: {{ .Values.nms.securityContext.runAsUser }}
        fsGroup: {{ .Values.nms.securityContext.fsGroup }}
      volumes:
        - name: secrets
          secret:
            secretName: {{ template "nms.fullname" . }}
