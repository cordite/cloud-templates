
> As the maintainers of Cordite, we value every community member's positive contribution of time and effort. We made a choice to open source the code making it free and available for everyone. As the use of Cordite grows so does the demands for support and maintenance. We have introduced a Cordite subscription to be able to provide support and maintenance to those organisations who are reliant on Cordite. The subscription will prioritise your issues and answer your calls for support. It will also accelerate the roadmap and delivery of your feature requests. If you are intending to use Cordite in production we would recommend your company evaluates the value of a Cordite subscription. If this is of interest to you then please drop an email to community@cordite.foundation 


# Cordite Cloud Templates

## Helm & Kubernetes
We have found [Helm](https://helm.sh/) is the best way to find, share, and use software built for [Kubernetes](https://kubernetes.io/). Kubernetes (K8s) is an open-source system for automating deployment, scaling, and management of containerized applications. You will find Kubernetes is available on all good Cloud providers.  

Lovers of homebrew can use `brew install kubernetes-helm` followed by `helm init`. For everyone else head over to [helm install instructions](https://github.com/helm/helm#install)  

Don't have a Kubernetes cluster? Every new Google Cloud Platform (GCP) account receives [$300 in credit upon sign up](https://console.cloud.google.com/freetrial). Once signed up you can [create a cluster](https://console.cloud.google.com/kubernetes)

Want to test locally? You can use Docker Desktop (18.06+) for [MacOS](https://docs.docker.com/docker-for-mac/#kubernetes) or [Windows](https://docs.docker.com/docker-for-windows/#kubernetes), which comes with Kubernetes embedded.  

## Cordite Chart Repo
To add cordite chart repo:  
```
$ helm repo add cordite-charts https://cordite.gitlab.io/cloud-templates
"cordite-charts" has been added to your repositories
$ helm repo list
NAME          	URL                  
cordite-charts	https://cordite.gitlab.io/cloud-templates       
$            
```

## Cordite Network Map Service (NMS) Chart
To install from the repo - `helm install cordite-charts/cordite-nms`  
For more information and options head over to [cordite-nms readme](https://gitlab.com/cordite/cloud-templates/tree/master/cordite-nms/cordite-nms)  


## Cordite Node
Detailed information regarding node deployment can be found at [Cordite Repo README](https://gitlab.com/cordite/cordite#how-do-i-deploy-my-own-cordite-node)

Cordite provides decentralised economic and governance services. For more information go to  https://gitlab.com/cordite/cordite/

All the deprecated Cordite node cloud templates are in the [other directory](https://gitlab.com/cordite/cloud-templates/tree/master/other)

## Cordite

### How do I get in touch?
  + News is announced on [@We_are_Cordite](https://twitter.com/we_are_cordite)
  + More information can be found on [Cordite website](https://cordite.foundation)
  + We use #cordite channel on [Corda slack](https://slack.corda.net/) 
  + We informally meet at the [Corda London meetup](https://www.meetup.com/pro/corda/)

### What if something does not work?
We encourage you to raise any issues/bugs you find with these cloud-templates. Please follow the below steps before raising issues:
   1. Check on the [Issues backlog](https://gitlab.com/cordite/cloud-templates/issues) to make sure an issue on the topic has not already been raised
   2. Post your question on the #cordite channel on [Corda slack](https://slack.corda.net/)
   3. If none of the above help solve the issue, [raise an issue](https://gitlab.com/cordite/cloud-templates/issues/new?issue) following the contributions guide

### How do I contribute?
We welcome contributions both technical and non-technical with open arms! There's a lot of work to do here. The [Contributing Guide](https://gitlab.com/cordite/cordite/blob/master/contributing.md) provides more information on how to contribute.

### Who is behind Cordite?
Cordite is being developed by a group of financial services companies, software vendors and open source contributors. The project is hosted on here on GitLab. 

### What open source license has this been released under?
All software in this repository is licensed under the Apache License, Version 2.0 (the "License"); you may not use this software except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

