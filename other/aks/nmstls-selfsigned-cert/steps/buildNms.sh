#!/bin/bash
set -e 
set -o pipefail

echo "${cyan}Step1: Login to azure$reset"
az login

echo "${cyan}Step2: Set Env variables$reset"
source setEnv.sh
echo "NMS resource group is ${cyan}${RESOURCE_GROUP}${reset}"
echo "NMS version is ${cyan}${VERSION}${reset}"
echo "NMS cluster name is ${cyan}${CLUSTER_NAME}${reset}"
echo "NMS namespace is ${cyan}${NAMESPACE}${reset}"
echo "NMS resource name is ${cyan}${RESOURCE_NAME}${reset}"
echo "NMS node resource group is ${cyan}${NODE_RESOURCE_GROUP}${reset}"
echo "Set NMS service ip as static ${cyan}${STATIC_IP}${reset}"

# Get access credentials for a managed Kubernetes cluster 
echo "${cyan}Step3: Get access credentials for a managed Kubernetes cluster$reset"
az aks get-credentials --resource-group $RESOURCE_GROUP --name $CLUSTER_NAME

# Create namespace
echo "${cyan}Step4: Create namespace$reset"
kubectl create namespace $NAMESPACE

# Create persistant volume claim
echo "${cyan}Step5: Create PVC$reset"
./createPvc.sh

# Create static ip address
echo "${cyan}Step6: Create Static ip$reset"
az network public-ip create --name $RESOURCE_NAME --resource-group $RESOURCE_GROUP --allocation-method Static 

# Create service
echo "${cyan}Step7: Create load balancer service$reset"
./createService.sh

# Deploy network map
echo "${cyan}Step8: Deploy network map$reset"
./createNmsDeployment.sh

