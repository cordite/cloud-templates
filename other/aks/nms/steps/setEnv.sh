# echo colors
export cyan=`tput setaf 6`
export reset=`tput sgr0`
# Variables
export NAMESPACE=${1:-nms}
export RESOURCE_NAME=${2:-nms}
export RESOURCE_GROUP=${3:-<RGNAME>} 
export VERSION=${4:-latest}
export CLUSTER_NAME=${5:-<CLUSTERNAME>}
export STATIC_IP=${6:-false}
# Retrive node resource group. This is needed while creating load balancer
export NODE_RESOURCE_GROUP=$(az aks show --resource-group $RESOURCE_GROUP --name $CLUSTER_NAME --query 'nodeResourceGroup' -o tsv)
