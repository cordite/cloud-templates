## Getting started with aks
[AKS basic tutorial](https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough): Before getting into the network map deployment into Azure kubernetes, it is good to go through this tutorial to understand how AKS works.

I firstly recommend creating the cluster based on your needs following the tutorial as AKS cluster creation takes a while. Secondly, azure VMs are bit expensive so it is good to look into the region and the VM cost while creating the cluster nodes. The above tutorial link will guide through the cluster creation.Once the cluster is created, please note down the cluster name.

Below are the list of services that are being used from AKS for NMS deployment:
1. Namespace
2. Persistent Volume Claim
3. Service
4. Deployment  

We enable secure https as well as http version of nms. For https connection, you are allowed to bring your own domain specific SSL cert or allow NMS to create a self signed certificate for you. We have created different folders to make the functionalities clear.

| Folder  | Features |
| ------------- | ------------- |
| nms  | http version of nms. It has an option to specify static ip address for the load balancer service  |
| nmstls-selfsigned-cert  | https version of nms with self signed certs  |
| nmstls-valid-cert  | https version of nms with your domain specific certs  |

*In this example, we are going to look into http version of NMS deployment.*

### Assign values to the below fields in the steps/setEnv.sh or pass as arguments:

```NAMESPACE``` - Name, you would like to group your resources that can be shared with other teams in K8s [refer here.](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)  
```RESOURCE_NAME``` - Value that will be used as name for pvc, service and deployment
```RESOURCE_GROUP``` - Azure resource group name  
```VERSION ``` - Version of NMS that you like to deploy   
```CLUSTER_NAME``` - Azure cluster name   
```STATIC_IP``` - Set to true if you would like to have static ipaddress for load balancer

### Deploy NMS

Ensure you have azure cli installed.

Run the script ./buildNms.sh from the steps folder.

This script will perform the following:  
* Login to azure  
* Set Env variables   
* Get access credentials for Kubernetes cluster  
* Create namespace in k8s  
* Create persistant volume claim in k8s  
* Create static ip address if static_ip is set to true else this step is skipped in k8s  
* Create service in k8s  
* Create deployment in k8s  

### Browse Kubernetes console

First run the below command to set the correct permissioning role 
```kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard```  

Then run
```az aks browse --resource-group $RESOURCE_GROUP --name $CLUSTER_NAME```  

### Check the network map

In the k8s dashboard, choose your namespace and check whether the service and deployment are showing green tick. Then click on the service and you can see the loadbalancer ipaddress and port link. Click on it. You will be redirected to the NMS home page.

### Delete k8s resources
kubectl delete namespace $NAMESPACE
